package com.automation.test.demo.steps;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.logging.Logger;

import cucumber.api.TestStep;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.junit.Cucumber;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@RunWith(Cucumber.class)
public class StepDefination
{
    public static WebDriver driver;
    public static Properties Nameprop;
    public static Properties Xpathprop;
    public static Properties Cssprop;
    public static Properties Idprop;
    public static Properties Constantsprop;
    public static WebElement webElement;
    private static final Logger LOGGER = Logger.getLogger(TestStep.class.getName());

    //Properties değerleri burada okunur. gelen locator bilgisine bağlı olarak hangi element olduğu bilgisini iletir
    // ve o element bilgisini bir sonraki işlem için elinde tutar.
    @Given("^import all elements$")
    public void import_all_elements() throws Throwable
    {

        Nameprop = new Properties();
        FileInputStream fisName = new FileInputStream(new File("name.properties").getAbsolutePath());
        System.out.println(fisName);
        Nameprop.load(fisName);

        Xpathprop = new Properties();
        FileInputStream fisXpath = new FileInputStream(new File("xpath.properties").getAbsolutePath());
        Xpathprop.load(fisXpath);

        Cssprop = new Properties();
        FileInputStream fisCss = new FileInputStream(new File("cssSelector.properties").getAbsolutePath());
        Cssprop.load(fisCss);

        Idprop = new Properties();
        FileInputStream fisId = new FileInputStream(new File("id.properties").getAbsolutePath());
        Idprop.load(fisId);

        Constantsprop = new Properties();
        FileInputStream fisConstant = new FileInputStream(new File("constants.properties").getAbsolutePath());
        Constantsprop.load(fisConstant);
    }

    //Browser seçimine göre setupları hazırlanır. Şimdilik chrome için bir örneğini yazdım.
    @Given("^open browser with \"([^\"]*)\"$")
    public void open_browser_with_option(String browserOption)
    {
        if (browserOption.equals("Chrome"))
        {
            String driverPath = "drivers/chromedriver.exe";
            System.setProperty("webdriver.chrome.driver", driverPath);
            System.out.println(driverPath);
            ChromeOptions options = new ChromeOptions();
            driver = new EventFiringWebDriver(new ChromeDriver(options));
        }

    }

    //Constantsprop propertisinden tanımlı url çağırılır.
    @Given("^navigate page \"([^\"]*)\"$")
    public void navigate_page_something(String pageUrl) throws Throwable
    {
        driver.navigate().to(Constantsprop.getProperty(pageUrl));
    }

    //seçili element clicklenir.
    @Given("^click element$")
    public void click_element()
    {
        webElement.click();
    }

    //Senaryo üzerinden parametrik olarak gönderilen texti orjinal text ile karşılaştırır ve textin doğruluğunu teyit eder.
    @Given("^assert element \"([^\"]*)\"$")
    public void assert_element(String text) throws Exception
    {

        String getConditionsTitle = webElement.getText();
        if (getConditionsTitle.equals(text))
        {
            Assert.assertTrue(getConditionsTitle, true);
        }
        if (!getConditionsTitle.equals(text))
        {
            Assert.assertTrue(getConditionsTitle, false);
            LOGGER.warning("Beklenen değer, gösterilen değerle eşleşemedi");
        }
    }

    //Bazı durumlarda javascript fonksiyonları normal clicklemeye izin vermez. bu durumda execute script ile element tıklanır.
    //Üyeliği tamamla butonu için yazıldı.
    //BUradaki logger kullanımı örnek olarak sunulmuştur.
    @Given("^click element with js$")
    public void click_element_with_js()
    {

        LOGGER.info("webElement degeri bulunuyor " + webElement);
        if (webElement != null)
        {
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].click();", webElement);
        }
        LOGGER.info("Üyeliği Tamamla butonuna tıklandı");
    }

    //Sayfayı tam ekran yapar.
    @And("^open max screen$")
    public void max_screen_element()

    {
        driver.manage().window().maximize();
    }

    //text alanlarına parametre olarak gönderdiğimiz değeri yazdırır.
    @And("^write text element \"([^\"]*)\"$")
    public void write_text_element_something(String text) throws Throwable
    {

        webElement.sendKeys(text);
    }

    //Senaryo steplerine eklendiği zaman bekleme, görünürlük ve tıklanabilirlik kontrol edilir.
    @And("^wait until the \"([^\"]*)\"$")
    public void wait_until_the_something(String option) throws Throwable
    {
        WebDriverWait wait = new WebDriverWait(driver, 100);
        if (option.equals("visibility"))
        {
            wait.until(ExpectedConditions.visibilityOf(webElement));
        }
        else if (option.equals("click"))
        {
            wait.until(ExpectedConditions.elementToBeClickable(webElement));
        }
        else if (option.equals("seconds"))
        {
            Thread.sleep(1000);
        }
    }

    @And("^find element \"([^\"]*)\"$")
    public void find_element_something(String element)
    {
        WebDriverWait wait = new WebDriverWait(driver, 100);

        if (Nameprop.getProperty(element) != null)
        {
            webElement = wait
                    .until(ExpectedConditions.visibilityOfElementLocated(By.name(Nameprop.getProperty(element))));
            System.out.println(By.name(Nameprop.getProperty(element)));
            webElement = driver.findElement(By.name(Nameprop.getProperty(element)));
        }
        else if (Cssprop.getProperty(element) != null)
        {
            webElement = wait
                    .until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(Cssprop.getProperty(element))));
            webElement = driver.findElement(By.cssSelector(Cssprop.getProperty(element)));
        }
        else if (Idprop.getProperty(element) != null)
        {
            webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(Idprop.getProperty(element))));
            webElement = driver.findElement(By.id(Idprop.getProperty(element)));

        }
        else if (Xpathprop.getProperty(element) != null)
        {
            webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Xpathprop.getProperty(element))));
            webElement = driver.findElement(By.xpath(Xpathprop.getProperty(element)));
        }
    }

    @Then("^browser close$")
    public void browser_close()
    {

        driver.close();
    }

}