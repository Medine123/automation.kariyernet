#Uyelik isimli bir feature dosyası oluşturup tüm case lerimizi gherkin dilinde buraya aktarıyoruz.
Feature: Uyelik

  Scenario: Uyeligi Olan Kullanici Ile Uyelik Olusturma
    Given import all elements
    Given open browser with "Chrome"
    Given navigate page "home_url"
    And open max screen
    And find element "isimElement"
    And write text element "abc"
    And find element "soyisimElement"
    And write text element "dgf"
    And find element "emailElement"
    And write text element "dgf@hshshs.com"
    And find element "sifreElement"
    And write text element "Abc12345"
    And find element "hizmetSozlesmesiOnay"
    And click element
    And find element "hizmetSozKabulBtn"
    And click element
    And find element "uyelikOlusturBtn"
    And click element with js
    And find element "captchaErrorId"
    And assert element "Lütfen ilgili alanı işaretleyin."
    And find element "email-error"
    And assert element "E-Posta adresi başka bir hesap tarafından kullanılıyor."
    Then browser close
    #senaryoların başında tag kullanarak içeride istediğimiz senaryoların çalışmasını sağlayabiliyoruz.
    #TestRunner classımıza bu tagı yazmamız gerekiyor.
  @calistir
  Scenario: Yeni Uyelik Olustur
    Given import all elements
    Given open browser with "Chrome"
    Given navigate page "home_url"
    And open max screen
    And find element "isimElement"
    And write text element "abc"
    And find element "soyisimElement"
    And write text element "dgf"
    And find element "emailElement"
    And write text element "dgf@hshshs.com"
    And find element "sifreElement"
    And write text element "Abc12345"
    And find element "hizmetSozlesmesiOnay"
    And click element
    And find element "hizmetSozKabulBtn"
    And click element
    And find element "uyelikOlusturBtn"
    And click element with js
    And find element "captchaErrorId"
    And assert element "Lütfen ilgili alanı işaretleyin."
    Then browser close

    #Validasyonlar için ayrı ayrı senaryolar oluşturulabilir.
    #Her bir validasyon için buarada bir senaryo düzenlenebilir.
    #örnek olması için tüm validasyonları tek senaryoda gösterdim.
  Scenario: Yeni Uyelik Validasyonlar
    Given import all elements
    Given open browser with "Chrome"
    Given navigate page "home_url"
    And open max screen
    And find element "uyelikOlusturBtn"
    And click element with js
    And find element "nameErrorId"
    And assert element "Adını yazmalısın."
    And find element "lastNameErrorId"
    And assert element "Soyadını yazmalısın."
    And find element "passwordErrorId"
    And assert element "Şifreni yazmalısın."
    And find element "captchaErrorId"
    And assert element "Lütfen ilgili alanı işaretleyin."
    Then browser close

  Scenario: Sifremi Unuttum
    Given import all elements
    Given open browser with "Chrome"
    Given navigate page "home_url"
    And open max screen
    And find element "uyeGirisYapLinkBtn"
    And click element
    And find element "sifremiUnuttum"
    And click element
    And find element "sifreUnuttumEpostaGonderText"
    And write text element "sshshhshs@dhdhdhd.com"
    And find element "sifreUnuttumEpostaGonderBtn"
    And click element
    Then browser close

  @calistir
  Scenario: Uye Girisi
    Given import all elements
    Given open browser with "Chrome"
    Given navigate page "home_url"
    And open max screen
    And find element "uyeGirisYapLinkBtn"
    And click element
    And find element "kullaniciAdiVeyaEposta"
    And write text element "sshshhshs@dhdhdhd.com"
    And find element "uyeSifreElement"
    And write text element "Abc12345"
    And find element "uyeGirisiYapBtn"
    And click element

  Scenario: Uye Girisi Validasyonlar
    Given import all elements
    Given open browser with "Chrome"
    Given navigate page "home_url"
    And open max screen
    And find element "uyeGirisYapLinkBtn"
    And click element
    And find element "uyeGirisiYapBtn"
    And click element
    And assert element "En az 3 karakter girmelisin"
    Then browser close