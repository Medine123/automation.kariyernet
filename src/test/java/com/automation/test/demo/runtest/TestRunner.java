package com.automation.test.demo.runtest;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
        //CucumberOptions seçenekleri ile case lerimizi feature lardan çekip buradan tetikleyebiliyoruz.
        //Bu kısımda ihtiyaca göre yeni seçenekler eklenebilir
        //Cucumber kütüphanesinin başlıca kullanım amacı otomatize ettiğimiz case leri daha rahat bir dille yazıp raporlayabiliyor olmaktır.
        features = "src/test/java/com/automation/test/demo/features",
        glue = "com.automation.test.demo.",
        //step notification kısmı senaryo steplerini görmemizi sağlıyor. bu kısım kaldırıldığı zaman case ler senaryo başlıkları halinde görünür.
        junit = "--step-notifications",
        strict = true,
        monochrome = true,
        //Buraya eklenen tag ile feature içerisinde istediğimiz senaryoları koşabiliyoruz.
        //tags satırını silersek içerideki tüm senaryolar çalışır.
        tags = "@calistir",
        //Plugin, test sonuçlarını aşağıdaki formatlarda raporlamamızı sağlıyor.
        plugin = {"pretty", "html:target/cucumber", "json:target/cucumber.json", "junit:target/cukes.xml"})
public class TestRunner
{

}
                                                              
